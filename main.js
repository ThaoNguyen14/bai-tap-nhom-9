var myFunc = function () {
  // Toogle Icon
  var node = document.getElementById("theme-toggler");
  if (node) {
    node.classList.toggle("is-active");
  }

  // Toogle HTML data-theme
  var html = document.getElementById("html");

  // Lay data color tu the html
  var theme = html.dataset.color;
  // neu theme = light -> doi theme thanh dark
  if (theme === "light") {
    // doi thanh dark
    html.dataset.color = "dark";
  }
  // nguoc lai doi theme thanh light
  else {
    // doi thanh light
    html.dataset.color = "light";
  }
};

var className = "inverted";
var scrollTrigger = 60;

window.onscroll = function () {
  // We add pageYOffset for compatibility with IE.
  if (window.scrollY >= scrollTrigger || window.pageYOffset >= scrollTrigger) {
    document.getElementsByTagName("header")[0].classList.add(className);
  } else {
    document.getElementsByTagName("header")[0].classList.remove(className);
  }

  // BACK TO TOP
  // Nút back to top sẽ xuất hiện khi user scroll từ đầu trang xuống 20px
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("movetop").style.display = "block";
  } else {
    document.getElementById("movetop").style.display = "none";
  }
}

// khi user click vào button, nó sẽ scroll lên đầu trang
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
